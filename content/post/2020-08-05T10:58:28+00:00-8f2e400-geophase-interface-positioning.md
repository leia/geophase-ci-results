---
title: commit-ID 8f2e400
date: 2020-08-05T10:58:28+00:00
--- 
# CI: correct path and title | 
# Iterative Volume-of-Fluid interface positioning in general polyhedrons with Consecutive Cubic Spline interpolation

### Publication

### Data 

**Endo-dodecahedron images**: http://dx.doi.org/10.25534/tudatalib-63

**Data:** 

### Source code

**Singularity Image:** 

**Git Repository:** https://git.rwth-aachen.de/leia/geophase

**Git Tag:** JCOMP-D-19-01329R1


```javascript
%%javascript
MathJax.Hub.Config({
    TeX: { equationNumbers: { autoNumber: "AMS" } }
});
```


    <IPython.core.display.Javascript object>


### IMPORTANT: set DATA_DIR to the relative path with respect to this notebook, that contains output data

***build*** is the default option as the tests are configured and executed in the build directory by CTest.


```python
%matplotlib inline
from plot_positioning import *
import seaborn as sns

DATA_DIR="../build" # !!! Path to the directory with test output. 
TEST_TYPE="CONVERGENCE" 
figPathName = os.curdir 
if "GEOP" in os.environ:
    figPathName = os.path.join(os.environ["GEOP"], "figures") 
```

## Visualization of the volume fraction of an endo-dodecahedron

Images of the intersected endo dodecahedros are available at [http://dx.doi.org/10.25534/tudatalib-63](http://dx.doi.org/10.25534/tudatalib-63).


```python
from matplotlib import offsetbox
from matplotlib import pyplot as plt
from matplotlib import rcParams
import pandas as pd
import urllib.request
import tarfile

rcParams["figure.dpi"] = 200
rcParams["figure.figsize"] = 4.5,3.5
alphaEndo = pd.read_csv('%s/UNIT_VOL_FRACTION.ENDO_DODECAHEDRON.csv' % DATA_DIR, comment="#")
rcParams['font.size'] = 11
rcParams["xtick.labelsize"] = 11

fig, ax = plt.subplots()
ax.grid(False)

ax.set_ylabel("$\\alpha_c(s)$")
ax.set_xlabel("$s$")

colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

def plot_polyhedron(N, xScale, yScale, ax):
    img = plt.imread("UNIT_VOL_ENDO_DODECAHEDRON.%04d.png" % N)
    img = offsetbox.OffsetImage(img, zoom=0.09)
    img.image.axes = ax
    ab = offsetbox.AnnotationBbox(img, xy=(alphaEndo['S'].loc[N], 
                                  alphaEndo['ALPHA_STAR'].loc[N]),
                                  xybox=(xScale*alphaEndo['S'].loc[N],yScale*alphaEndo['ALPHA_STAR'].loc[N]),
                                  frameon=False, 
                                  arrowprops=dict(arrowstyle="-")) 
    ax.add_artist(ab)      

plot_polyhedron(5, 2, 0.6, ax)
plot_polyhedron(50,0.65, 0.35, ax)
plot_polyhedron(95, 0.7, 3, ax)
ax.plot(alphaEndo['S'], alphaEndo['ALPHA_STAR'], lw=2, label=r"$\alpha_c(s)$")
ax.legend()

figBaseName = "ENDO_DODECAHEDRON_ALPHA_S"
fig.savefig(figBaseName + ".png", dpi=400)
fig.savefig(figBaseName + ".pdf", dpi=400)

if "GEOP" in os.environ:
    pathName = os.path.join(os.environ["GEOP"], "figures", figBaseName)
    if (os.path.exists(pathName)):
        fig.savefig(pathName + ".png", dpi=400)
        fig.savefig(pathName + ".pdf", dpi=400)
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_5_0.png)


### Average number of iterations and CPU times for different polyhedrons (cells)

#### Computing architecture

**CPU**

vendor id : AuthenticAMD, cpu family : 23, model : 24, model name : AMD Ryzen 7 PRO 3700U w/ Radeon Vega Mobile Gfx

Output from ”cpupower frequency-info”: ”[CPU] Frequency should be within 2.30 GHz and 2.30 GHz.
The governor ”performance” may decide which speed to use within this range.”

**Compiler**

version : g++ 9.3.0-1, optimization flags : -std=c++2a -O3


```python
dataFileName = "%s/%s_TEST_POSITIONING_TOTAL_DATA.csv" % (DATA_DIR, TEST_TYPE)
totalData = pd.read_csv(dataFileName, comment='#') 
plot_total_data(totalData)
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_7_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_7_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_7_2.png)


# 1 Tetrahedron 

## 1.1 Tetrahedron: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.TETRAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataTetrahedron = pd.read_csv("%s/%s" % (DATA_DIR, testName + ".csv"), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataTetrahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_10_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_10_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_10_2.png)


## 1.2 Tetrahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.TETRAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataTetrahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataTetrahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_12_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_12_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_12_2.png)


## 1.3 Tetrahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataTetrahedron, newtonDataTetrahedron, 'Tetrahedron')
plt.grid(False)
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_14_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataTetrahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataTetrahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("TETRAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_TETRAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_15_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataTetrahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
cubicCpuTimeAlphaMean = cubicDataTetrahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$")
plt.title("Average CPU time: TETRAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_TETRAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_17_0.png)


# 2 Cube 

## 2.1 Cube: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.CUBE_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataCube = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataCube, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_20_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_20_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_20_2.png)


## 2.2 Cube: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.CUBE_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataCube = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataCube, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_22_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_22_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_22_2.png)


## 2.3 Cube: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataCube, newtonDataCube, 'Cube')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_24_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataCube.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataCube.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("CUBE",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_CUBE_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_25_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataCube.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
cubicCpuTimeAlphaMean = cubicDataCube.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$")
plt.title("Average CPU time: CUBE ")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_CUBE_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_27_0.png)


# 3 Docehahedron 

## 3.1 Dodecahedron: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.DODECAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataDodecahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_30_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_30_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_30_2.png)


## 3.2 Dodecahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.DODECAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataDodecahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_32_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_32_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_32_2.png)


## 3.3 Dodecahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataDodecahedron, newtonDataDodecahedron, 'Dodecahedron')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_34_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("DODECAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_DODECAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_35_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataDodecahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
cubicCpuTimeAlphaMean = cubicDataDodecahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha^*$")
plt.title("Average CPU time: DODECAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_DODECAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_37_0.png)


# 4 Non-planar dodecahedron

## 4.1 Non-planar dodecahedron: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.NON_PLANAR_DODECAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataNplDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR,testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataNplDodecahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_40_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_40_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_40_2.png)


## 4.2 Non-planar dodecahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.NON_PLANAR_DODECAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataNplDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataNplDodecahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_42_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_42_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_42_2.png)


## 4.3 Non-planar dodecahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataNplDodecahedron, newtonDataNplDodecahedron, 'Non-planar dodecahedron')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_44_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataNplDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataNplDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("NON-PLANAR DODECAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_NON_PLANAR_DODECAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_45_0.png)


**CPU Time** 


```python
set_iteration_fig_properties()
newtonCpuTimeAlphaMean = newtonDataNplDodecahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
cubicCpuTimeAlphaMean = cubicDataNplDodecahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha^*$")
plt.title("Average CPU time: NON-PLANAR DODECAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_NON_PLANAR_DODECAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_47_0.png)


# 5 Endo-dodecahedron

## 5.1 Endo-dodecahedron: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.ENDO_DODECAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataEndoDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR,testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataEndoDodecahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_50_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_50_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_50_2.png)


## 5.2 Endo-dodecahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.ENDO_DODECAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataEndoDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR,testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataEndoDodecahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_52_0.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_52_1.png)



![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_52_2.png)


## 5.3 Endo-dodecahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataEndoDodecahedron, newtonDataEndoDodecahedron, 'Endo dodecahedron')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_54_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataEndoDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataEndoDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("ENDO-DODECAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_ENDO_DODECAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_55_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataEndoDodecahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
cubicCpuTimeAlphaMean = cubicDataEndoDodecahedron.groupby('ALPHA_STAR').CPU_TIME_MICROSECONDS.mean()
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$")
plt.title("Average CPU time: ENDO-DODECAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_ENDO_DODECAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-08-05T10:58:28+00:00-8f2e400-geophase-interface-positioning_files/geophase-interface-positioning_57_0.png)

