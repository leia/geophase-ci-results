---
title: commit-ID 32b344a
date: 2020-12-04T11:47:36+00:00
--- 
# CI update for new data folder structure | 
# Iterative Volume-of-Fluid interface positioning in general polyhedrons with Consecutive Cubic Spline interpolation

### Data 

**Endo-dodecahedron images**: http://dx.doi.org/10.25534/tudatalib-63

### Source code

**Git Repository:** https://git.rwth-aachen.de/leia/geophase

**Git Tag:** JCOMP-D-19-01329R2


```javascript
%%javascript
MathJax.Hub.Config({
    TeX: { equationNumbers: { autoNumber: "AMS" } }
});
```


    <IPython.core.display.Javascript object>


### IMPORTANT: set DATA_DIR to the relative path with respect to this notebook, that contains output data

***build-timing*** is the default option 


```python
%matplotlib inline
from plot_positioning import *
import seaborn as sns

DATA_DIR="../timing-data" # !!! Path to the directory with test output. 
TEST_TYPE="CONVERGENCE" 
figPathName = os.curdir 
if "GEOP" in os.environ:
    figPathName = os.path.join(os.environ["GEOP"], "figures") 
```


```python
from matplotlib import offsetbox
from matplotlib import pyplot as plt
from matplotlib import rcParams
import pandas as pd
import urllib.request
import tarfile

rcParams["figure.dpi"] = 200
rcParams["figure.figsize"] = 4.5,3.5
rcParams['font.size'] = 11
rcParams["xtick.labelsize"] = 11
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
```

### Average number of iterations and CPU times for different polyhedrons (cells)

#### Computing architecture

**CPU**

vendor id : AuthenticAMD, cpu family : 23, model : 24, model name : AMD Ryzen 7 PRO 3700U w/ Radeon Vega Mobile Gfx

Output from ”cpupower frequency-info”: ”[CPU] Frequency should be within 2.30 GHz and 2.30 GHz.
The governor ”performance” may decide which speed to use within this range.”

**Compiler**

version : g++ 9.3.0-1, optimization flags : -std=c++2a -O3


```python
dataFileName = "%s/%s_TEST_POSITIONING_TOTAL_DATA.csv" % (DATA_DIR, TEST_TYPE)
totalData = pd.read_csv(dataFileName, comment='#') 
plot_total_data(totalData)
```

    Saving fig



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_6_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_6_2.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_6_3.png)


# 1 Tetrahedron 

## 1.1 Tetrahedron: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.TETRAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataTetrahedron = pd.read_csv("%s/%s" % (DATA_DIR, testName + ".csv"), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataTetrahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_9_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_9_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_9_2.png)


## 1.2 Tetrahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.TETRAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataTetrahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataTetrahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_11_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_11_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_11_2.png)


## 1.3 Tetrahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataTetrahedron, newtonDataTetrahedron, 'Tetrahedron')
plt.grid(False)
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_13_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataTetrahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataTetrahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("TETRAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_TETRAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_14_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataTetrahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
cubicCpuTimeAlphaMean = cubicDataTetrahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$")
plt.title("Average CPU time: TETRAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_TETRAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_16_0.png)


# 2 Cube 

## 2.1 Cube: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.CUBE_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataCube = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataCube, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_19_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_19_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_19_2.png)


## 2.2 Cube: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.CUBE_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataCube = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataCube, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_21_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_21_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_21_2.png)


## 2.3 Cube: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataCube, newtonDataCube, 'Cube')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_23_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataCube.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataCube.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("CUBE",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_CUBE_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_24_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataCube.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
cubicCpuTimeAlphaMean = cubicDataCube.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$")
plt.title("Average CPU time: CUBE ")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_CUBE_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_26_0.png)


# 3 Docehahedron 

## 3.1 Dodecahedron: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.DODECAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataDodecahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_29_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_29_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_29_2.png)


## 3.2 Dodecahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.DODECAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataDodecahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_31_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_31_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_31_2.png)


## 3.3 Dodecahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataDodecahedron, newtonDataDodecahedron, 'Dodecahedron')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_33_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("DODECAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_DODECAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_34_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataDodecahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
cubicCpuTimeAlphaMean = cubicDataDodecahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha^*$")
plt.title("Average CPU time: DODECAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_DODECAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_36_0.png)


# 4 Non-planar dodecahedron

## 4.1 Non-planar dodecahedron: Newton Cubic Spline Method


```python
testName = "%s_TEST_POSITIONING.NON_PLANAR_DODECAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataNplDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR,testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataNplDodecahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_39_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_39_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_39_2.png)


## 4.2 Non-planar dodecahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.NON_PLANAR_DODECAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataNplDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR, testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataNplDodecahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_41_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_41_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_41_2.png)


## 4.3 Non-planar dodecahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataNplDodecahedron, newtonDataNplDodecahedron, 'Non-planar dodecahedron')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_43_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataNplDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataNplDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("NON-PLANAR DODECAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_NON_PLANAR_DODECAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_44_0.png)


**CPU Time** 


```python
set_iteration_fig_properties()
newtonCpuTimeAlphaMean = newtonDataNplDodecahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
cubicCpuTimeAlphaMean = cubicDataNplDodecahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha^*$")
plt.title("Average CPU time: NON-PLANAR DODECAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_NON_PLANAR_DODECAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_46_0.png)


# 5 Endo-dodecahedron


```python
testName = "%s_TEST_POSITIONING.ENDO_DODECAHEDRON_NEWTON_CUBIC_SPLINE" % TEST_TYPE
newtonDataEndoDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR,testName), comment='#', index_col=[0,1,2])
plot_iterations(newtonDataEndoDodecahedron, testName, "NEWTON CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_48_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_48_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_48_2.png)


## 5.2 Endo-dodecahedron: Consecutive Cubic Spline 


```python
testName = "%s_TEST_POSITIONING.ENDO_DODECAHEDRON_CONSECUTIVE_CUBIC_SPLINE" % TEST_TYPE
cubicDataEndoDodecahedron = pd.read_csv("%s/%s.csv" % (DATA_DIR,testName), comment='#', index_col=[0,1,2])
plot_iterations(cubicDataEndoDodecahedron, testName, "CONSECUTIVE CUBIC SPLINE")
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_50_0.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_50_1.png)



![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_50_2.png)


## 5.3 Endo-dodecahedron: Method comparison 
**Iterations**


```python
plot_iteration_boxplots(cubicDataEndoDodecahedron, newtonDataEndoDodecahedron, 'Endo-dodecahedron')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_52_0.png)



```python
set_iteration_fig_properties()
newtonAlphaItMean = newtonDataEndoDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
cubicAlphaItMean = cubicDataEndoDodecahedron.groupby('ALPHA_STAR').ITERATIONS.mean()
plt.plot(newtonAlphaItMean.index, newtonAlphaItMean, marker='*', label="NEWTON CUBIC SPLINE")
plt.plot(cubicAlphaItMean.index, cubicAlphaItMean, marker='*', label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$", fontsize=11)
plt.title("ENDO-DODECAHEDRON",fontsize=11)
plt.ylabel("Average iterations",fontsize=11)
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_ENDO_DODECAHEDRON_ITERATIONS.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_53_0.png)


**CPU Time** 


```python
newtonCpuTimeAlphaMean = newtonDataEndoDodecahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
cubicCpuTimeAlphaMean = cubicDataEndoDodecahedron.groupby('ALPHA_STAR').CPU_TIME_NANOSECONDS.mean() / 1000.
plt.plot(newtonCpuTimeAlphaMean.index, newtonCpuTimeAlphaMean, marker="*", label="NEWTON CUBIC SPLINE")
plt.plot(cubicCpuTimeAlphaMean.index, cubicCpuTimeAlphaMean, marker="*", label="CONSECUTIVE CUBIC SPLINE")
plt.xlabel(r"$\alpha_c$")
plt.title("Average CPU time: ENDO-DODECAHEDRON")
plt.ylabel("Average CPU time in microseconds")
plt.legend()
plt.savefig(os.path.join(figPathName,"COMPARISON_NCS_CCS_ENDO_DODECAHEDRON_CPU_TIME.pdf"),
            bbox_inches='tight')
```


![png](/post/2020-12-04T11:47:36+00:00-32b344a-geophase-interface-positioning_files/geophase-interface-positioning_55_0.png)

